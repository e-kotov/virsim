#include <RcppArmadillo.h>
// [[Rcpp::depends("RcppArmadillo")]]

using namespace Rcpp;
using namespace arma;

//' @importFrom Rcpp evalCpp
//' @useDynLib virsim, .registration = TRUE
//' @export
// [[Rcpp::export]]
arma::vec group_means (const arma::vec& cum_x,
                       const arma::uvec& group) {
  unsigned int l = group.n_rows;
  arma::uvec ind = find(group.rows(1, l - 1) != group.rows(0, l - 2));
  unsigned int k = ind.n_rows;
  ind.insert_rows(k, 1);
  ind.at(k) = l - 1;
  //
  arma::vec out = cum_x.elem(ind);
  out.insert_rows(0, 1);
  unsigned int n = out.n_elem;
  out = out.rows(1, n - 1) - out.rows(0, n - 2);
  //
  arma::vec ns = conv_to<vec>::from(ind);
  arma::vec diff_ns = diff(ns);
  diff_ns.insert_rows(0, 1);
  diff_ns.at(0) = ns.at(0) + 1;
  //
  out.each_col() /= diff_ns;
  return(out);
}

// Assign new infections --------------------------------------------------------
//' Assign new infections
//'
//' Simulate transmission of infection from infectious to susceptible cases.
//' N.B., all individuals belonging to a cluster or supercluster are assumed to
//' be next to each other in each vector and to be ordered exactly the same in
//' all input vectors!
//'
//' @param infectious Logical vector indicating who is infectious.
//' @param susceptible Logical vector indicating who is susceptible.
//' @param rel_cr_eff Numerical vector of individual effective relative contact
//'   rates.
//' @param trace_effect Numerical vector of individual effects of being traced.
//' @param mix_reduction Numerical vector of individual effects due to
//'   supercluster-specific reductions in population-level mixing.
//' @param cluster Integer vector of clusters that individuals belong to.
//' @param supercluster Integer vector of superclusters that individuals belong
//'   to.
//' @param contact_rate Contact rate.
//' @param population_mixing Degree of mixing in entire population.
//' @param supercluster_mixing Degree of mixing within superclusters.
//' @param efoi External force of infection.
//' @param dt Time step size
//'
//' @return This function returns an integer vector of zeroes and ones
//'   indicating new infections.
//'
//' @importFrom Rcpp evalCpp
//' @useDynLib virsim, .registration = TRUE
//' @export
// [[Rcpp::export]]
arma::uvec transmit (const arma::vec& trace_effect,
                     const arma::vec& rel_cr_eff,
                     const arma::vec& infectious,
                     const arma::vec& susceptible,
                     const arma::vec& mix_reduction,
                     const arma::uvec& cluster,
                     const arma::uvec& supercluster,
                     const double& population_mixing,
                     const double& supercluster_mixing,
                     const double& dt,
                     const double& contact_rate,
                     const double& efoi) {
  arma::vec A = trace_effect % rel_cr_eff % infectious;
  arma::vec cumsum_A = cumsum(A);
  arma::vec foi_cluster = group_means(cumsum_A, cluster) *
    (1 - population_mixing - supercluster_mixing);
  arma::vec foi_sc = group_means(cumsum_A, supercluster) * supercluster_mixing;
  double foi_pop = population_mixing * mean(mix_reduction % A);
  arma::vec foi = foi_cluster.elem(cluster - 1) +
    foi_sc.elem(supercluster - 1) + mix_reduction * foi_pop;
  arma::vec probs = - expm1(-dt * contact_rate *
    (rel_cr_eff % trace_effect % (foi + efoi)));
  int n = probs.n_rows;
  return(randu<vec>(n) < probs);
}

