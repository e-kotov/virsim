# Generate a phased lift control programme -------------------------------------
#' Generate a phased lift control programme
#'
#' Generate a schedule for phased lift of lock-down in superclusters, while
#' potentially quarantining superclusters that are just coming out of lock-down.
#'
#' @param intervention_start Start of lock-down (day).
#' @param init_lockdown_dur Duration of initial complete lock-down (days).
#' @param lockdown_effect The reduction in transmission rate (individual
#'   relative contact rates are multiplied with the square root of this number)
#'   during the initial lockdown.
#' @param interventions_lift_interval Interval (days) between lift of lock-down
#'   in consecutive superclusters.
#' @param extra_duration_first_interval Extra duration of the interval between
#'   lift of lock-down in the first and second supercluster (i.e. duration in
#'   addition to \code{interventions_lift_interval}).
#' @param start_reduce_interval The n-th supercluster from which onwards the
#'   interval is reduced by \code{reduce_interval}.
#' @param repeat_reduction The duration (number of phases) between
#'   cumulative reductions in the interval.
#' @param reduce_interval The number of days by which intervals are reduced from
#'   the \code{start_reduce_interval}-th interval.
#' @param pl_intervention_effect_hi The reduction in transmission rate (individual
#'   relative contact rates are multiplied with the square root of this number)
#'   during phased lift.
#' @param pl_intervention_effect_lo The reduction in transmission rate (individual
#'   relative contact rates are multiplied with the square root of this number)
#'   after lift of lock-down.
#' @param uptake The proportion of the population that takes up the
#'   intervention.
#' @param n_phases Number of phases in which to lift lock-down. This must be
#'   smaller than or equal to the number of superclusters in the population.
#' @param n_supercluster Number of superclusters.
#' @param sc_isolation_effect A multiplier for the contribution and exposure of
#'   a supercluster where lock-down was just lifted.
#' @param lockdown_isol Logical flag indicating whether the
#'   \code{sc_isolation_effect} should also be applied during initial lock-
#'   down or only for superclusters that start phased release
#' @return Returns a \code{list} with definitions for intervention-related
#'   arguments to \code{virsim}: \code{intervention_t},
#'   \code{intervention_uptake}, \code{intervention_effect}, and
#'   \code{sc_isolation_effect}.
#'
#' @export
gen_phased_lift <- function(intervention_start = 30,
                            init_lockdown_dur = 60,
                            lockdown_effect = 0.20,
                            interventions_lift_interval = 45,
                            extra_duration_first_interval = 15,
                            start_reduce_interval = 4,
                            repeat_reduction = 2,
                            reduce_interval = 0,
                            pl_intervention_effect_hi = 0.25,
                            pl_intervention_effect_lo = 1,
                            uptake = 1,
                            n_phases = n_supercluster,
                            n_supercluster = 20,
                            sc_isolation_effect = 0.5,
                            lockdown_isol = FALSE,
                            ...) {

  if (n_phases > n_supercluster) stop("n_phases must be <= n_supercluster")

  # Construct time schedule of phased lifting of control per supercluster
  start_times <-
    intervention_start + init_lockdown_dur +
    c(0, extra_duration_first_interval +
        interventions_lift_interval * (1:(n_phases - 1)))

  if (start_reduce_interval < n_phases) {
    t_reduction <- seq(start_reduce_interval, n_phases, by = repeat_reduction)
    t_reduction <- t_reduction[t_reduction < n_phases]
    if (is.list(reduce_interval)) {
      reduce_interval <- unlist(reduce_interval, recursive = FALSE)
    }
    if (length(reduce_interval) < length(t_reduction)) {
      reduce_interval <- rep(reduce_interval, length(t_reduction))
    }
    interval_reduction <- rep(0, n_phases)
    for(t in 1:length(t_reduction)) {
      interval_reduction <- interval_reduction + reduce_interval[t] *
        c(rep(0, t_reduction[t]), 1:(n_phases - t_reduction[t]))
    }
    start_times <- start_times - interval_reduction
  }

  intervention_t <- c(0, intervention_start, start_times)
  intervention_uptake <- matrix(0,
                                nrow = n_phases,
                                ncol = length(intervention_t),
                                dimnames = list(NULL,
                                                intervention_t))
  intervention_effect <- matrix(0,
                                nrow = n_phases,
                                ncol = length(intervention_t),
                                dimnames = list(NULL,
                                                intervention_t))

  intervention_uptake[, -c(1, length(intervention_t))] <- uptake
  intervention_effect[, -1] <- pl_intervention_effect_hi
  intervention_effect[, 2] <- lockdown_effect

  for (t in 1:n_phases) {
    start_index <- which(intervention_t == start_times[t])
    intervention_effect[t, start_index:length(intervention_t)] <- pl_intervention_effect_lo
  }

  isolation_effect <- matrix(1,
                             nrow = n_phases,
                             ncol = length(intervention_t),
                             dimnames = list(NULL,
                                             intervention_t))
  diag(isolation_effect[,-(1:2)]) <- sc_isolation_effect
  if (lockdown_isol == TRUE) {
    isolation_effect[, 2] <- sc_isolation_effect
  }

  # Finish setup
  setup <- rep(ceiling(n_supercluster / n_phases), n_phases)
  if (sum(setup) > n_supercluster) {
    setup[length(setup)] <- setup[length(setup)] - (sum(setup) - n_supercluster)
  }
  setup <- rep(1:length(setup), setup)
  intervention_uptake <- intervention_uptake[setup, ]
  intervention_effect <- intervention_effect[setup, ]
  isolation_effect <- isolation_effect[setup, ]

  # Collate and return
  list(intervention_t = intervention_t,
       intervention_uptake = intervention_uptake,
       intervention_effect = intervention_effect,
       sc_isolation_effect = isolation_effect)

}


### END OF CODE ###
